const { gitlabAPI } = require("./common");

const getBranchBase = async (commit) => {
  const { data: commits } = await gitlabAPI.get(
    `/repository/commits?ref_name=master..${commit}`
  );

  if (commits.length === 1) {
    return commits[0].parent_ids[0];
  }

  if (commits.length > 1) {
    return await getBranchBase(commits.reverse()[0].id);
  }

  throw new Error("Cannot find a base commit");
};

module.exports = {
  getCommits: (params) =>
    gitlabAPI.get("/repository/commits", {
      params,
    }),
  getJob: (id, params = {}) =>
    gitlabAPI.get(`/jobs/${id}`, {
      params,
    }),
  getBranchBase,
};
