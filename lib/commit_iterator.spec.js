const moment = require("moment");

const { getCommits } = require("./gitlab_api");

const CommitIterator = require("./commit_iterator");

jest.mock("./gitlab_api");

const mockCommits = [
  {
    id: "9eb2d34fb07b1ff4fc65c6a9cc74c8d8225e3d87",
    short_id: "9eb2d34f",
    created_at: "2020-05-20T18:55:23.000+00:00",
    parent_ids: [
      "8253d022cbe868e043404054332c32f0525e2467",
      "80e06c31591117cc1b330816e07c0e58f93a4c6d",
    ],
    title: "Merge branch 'docs-codeowners-parent' into 'master'",
    message:
      "Merge branch 'docs-codeowners-parent' into 'master'\n\nAdd support for including user/groups from parent/ancestors\n\nSee merge request gitlab-org/gitlab!32353",
    author_name: "Marcia Ramos",
    author_email: "marcia@gitlab.com",
    authored_date: "2020-05-20T18:55:23.000+00:00",
    committer_name: "Marcia Ramos",
    committer_email: "marcia@gitlab.com",
    committed_date: "2020-05-20T18:55:23.000+00:00",
    web_url:
      "https://gitlab.com/gitlab-org/gitlab/-/commit/9eb2d34fb07b1ff4fc65c6a9cc74c8d8225e3d87",
  },
  {
    id: "8253d022cbe868e043404054332c32f0525e2467",
    short_id: "8253d022",
    created_at: "2020-05-20T18:38:22.000+00:00",
    parent_ids: [
      "5841b44b0f6d370f1edbbd5aa2e172ff9de9cb63",
      "fa9028e0cf005a327055d65cbe3ee1e471b5f2a0",
    ],
    title: "Merge branch 'dmishunov-fixing-jest-config-for-ides' into 'master'",
    message:
      "Merge branch 'dmishunov-fixing-jest-config-for-ides' into 'master'\n\nFixing the JEST config for IDEs\n\nSee merge request gitlab-org/gitlab!32546",
    author_name: "Paul Slaughter",
    author_email: "pslaughter@gitlab.com",
    authored_date: "2020-05-20T18:38:22.000+00:00",
    committer_name: "Paul Slaughter",
    committer_email: "pslaughter@gitlab.com",
    committed_date: "2020-05-20T18:38:22.000+00:00",
    web_url:
      "https://gitlab.com/gitlab-org/gitlab/-/commit/8253d022cbe868e043404054332c32f0525e2467",
  },
  {
    id: "5841b44b0f6d370f1edbbd5aa2e172ff9de9cb63",
    short_id: "5841b44b",
    created_at: "2020-05-20T17:40:21.000+00:00",
    parent_ids: [
      "e437426bc123bb91255befbf94316c290f78aced",
      "b301c6ad5e828a2707a480326bc21dcff7a439f9",
    ],
    title:
      "Merge branch '217321-update-the-admin-audit-events-sorting-dropdown-to-a-vue-app-which-utilises-gitlab-ui' into 'master'",
    message:
      "Merge branch '217321-update-the-admin-audit-events-sorting-dropdown-to-a-vue-app-which-utilises-gitlab-ui' into 'master'\n\nChange Admin Audit Events sorting field to use GitLab UI\n\nCloses #217321\n\nSee merge request gitlab-org/gitlab!32299",
    author_name: "Natalia Tepluhina",
    author_email: "ntepluhina@gitlab.com",
    authored_date: "2020-05-20T17:40:21.000+00:00",
    committer_name: "Natalia Tepluhina",
    committer_email: "ntepluhina@gitlab.com",
    committed_date: "2020-05-20T17:40:21.000+00:00",
    web_url:
      "https://gitlab.com/gitlab-org/gitlab/-/commit/5841b44b0f6d370f1edbbd5aa2e172ff9de9cb63",
  },
  {
    id: "e437426bc123bb91255befbf94316c290f78aced",
    short_id: "e437426b",
    created_at: "2020-05-20T17:13:36.000+00:00",
    parent_ids: [
      "d3c771ea222e39f95b547e5349a1d66298e39ce3",
      "e7e770db1a8f390843da265ee188438eeffadba6",
    ],
    title: "Merge branch 'russell/edit-telemetry-docs' into 'master'",
    message:
      "Merge branch 'russell/edit-telemetry-docs' into 'master'\n\nEdited Telemetry docs per review\n\nSee merge request gitlab-org/gitlab!32615",
    author_name: "Suzanne Selhorn",
    author_email: "sselhorn@gitlab.com",
    authored_date: "2020-05-20T17:13:36.000+00:00",
    committer_name: "Suzanne Selhorn",
    committer_email: "sselhorn@gitlab.com",
    committed_date: "2020-05-20T17:13:36.000+00:00",
    web_url:
      "https://gitlab.com/gitlab-org/gitlab/-/commit/e437426bc123bb91255befbf94316c290f78aced",
  },
  {
    id: "d3c771ea222e39f95b547e5349a1d66298e39ce3",
    short_id: "d3c771ea",
    created_at: "2020-05-20T16:48:22.000+00:00",
    parent_ids: [
      "d12095b5355aa78862d7032f8a6cbece8fc2d999",
      "b93691a6d164916538772c982fee1a70d9da9f4e",
    ],
    title:
      "Merge branch '199388-propagate-instance-level-integration' into 'master'",
    message:
      "Merge branch '199388-propagate-instance-level-integration' into 'master'\n\nPropagate instance-level integration to projects\n\nSee merge request gitlab-org/gitlab!32331",
    author_name: "Adam Hegyi",
    author_email: "ahegyi@gitlab.com",
    authored_date: "2020-05-20T16:48:22.000+00:00",
    committer_name: "Adam Hegyi",
    committer_email: "ahegyi@gitlab.com",
    committed_date: "2020-05-20T16:48:22.000+00:00",
    web_url:
      "https://gitlab.com/gitlab-org/gitlab/-/commit/d3c771ea222e39f95b547e5349a1d66298e39ce3",
  },
  {
    id: "d12095b5355aa78862d7032f8a6cbece8fc2d999",
    short_id: "d12095b5",
    created_at: "2020-05-20T16:41:18.000+00:00",
    parent_ids: [
      "dddc9382a672e065131dce42821229c24783cbff",
      "49b390585ae33357e9a1aea9075669ee9c649b0c",
    ],
    title: "Merge branch 'add-runner-breaking-changes' into 'master'",
    message:
      "Merge branch 'add-runner-breaking-changes' into 'master'\n\nAdd all breaking changes for the Runner in 13.0\n\nSee merge request gitlab-org/gitlab!32445",
    author_name: "Suzanne Selhorn",
    author_email: "sselhorn@gitlab.com",
    authored_date: "2020-05-20T16:41:18.000+00:00",
    committer_name: "Suzanne Selhorn",
    committer_email: "sselhorn@gitlab.com",
    committed_date: "2020-05-20T16:41:18.000+00:00",
    web_url:
      "https://gitlab.com/gitlab-org/gitlab/-/commit/d12095b5355aa78862d7032f8a6cbece8fc2d999",
  },
  {
    id: "dddc9382a672e065131dce42821229c24783cbff",
    short_id: "dddc9382",
    created_at: "2020-05-20T16:01:27.000+00:00",
    parent_ids: [
      "a733c4a686b5164d728fa9a0b0e247d57df33228",
      "23cc1598391160e9c78c74ee715d9ee42c6c1519",
    ],
    title:
      "Merge branch 'alexives/35913/add_geo_pause_api_endpoint' into 'master'",
    message:
      "Merge branch 'alexives/35913/add_geo_pause_api_endpoint' into 'master'\n\nAdd geo node enable api\n\nSee merge request gitlab-org/gitlab!30547",
    author_name: "Robert Speicher",
    author_email: "rspeicher@gmail.com",
    authored_date: "2020-05-20T16:01:27.000+00:00",
    committer_name: "Robert Speicher",
    committer_email: "rspeicher@gmail.com",
    committed_date: "2020-05-20T16:01:27.000+00:00",
    web_url:
      "https://gitlab.com/gitlab-org/gitlab/-/commit/dddc9382a672e065131dce42821229c24783cbff",
  },
  {
    id: "a733c4a686b5164d728fa9a0b0e247d57df33228",
    short_id: "a733c4a6",
    created_at: "2020-05-20T15:55:00.000+00:00",
    parent_ids: [
      "e725a483d938046cb8529e726d3f1e0fbc87f84e",
      "97b6fe17053ff9a14d3e294decfe1e82f4366ede",
    ],
    title: "Merge branch 'leaky-constant-fix-14' into 'master'",
    message:
      "Merge branch 'leaky-constant-fix-14' into 'master'\n\nFix leaky constant in task completion status spec\n\nSee merge request gitlab-org/gitlab!32043",
    author_name: "Peter Leitzen",
    author_email: "pleitzen@gitlab.com",
    authored_date: "2020-05-20T15:55:00.000+00:00",
    committer_name: "Peter Leitzen",
    committer_email: "pleitzen@gitlab.com",
    committed_date: "2020-05-20T15:55:00.000+00:00",
    web_url:
      "https://gitlab.com/gitlab-org/gitlab/-/commit/a733c4a686b5164d728fa9a0b0e247d57df33228",
  },
  {
    id: "e725a483d938046cb8529e726d3f1e0fbc87f84e",
    short_id: "e725a483",
    created_at: "2020-05-20T15:30:24.000+00:00",
    parent_ids: [
      "d59a0f0e8e42ca56e58babf107958ca73d900a6e",
      "cc9b8a53695abf01540340b3eed58f86f25321d9",
    ],
    title: "Merge branch 'jlouw-add-flags-to-audit-filter' into 'master'",
    message:
      "Merge branch 'jlouw-add-flags-to-audit-filter' into 'master'\n\nAdd search token configuration to AuditLogFilter\n\nSee merge request gitlab-org/gitlab!32537",
    author_name: "Phil Hughes",
    author_email: "me@iamphill.com",
    authored_date: "2020-05-20T15:30:24.000+00:00",
    committer_name: "Phil Hughes",
    committer_email: "me@iamphill.com",
    committed_date: "2020-05-20T15:30:24.000+00:00",
    web_url:
      "https://gitlab.com/gitlab-org/gitlab/-/commit/e725a483d938046cb8529e726d3f1e0fbc87f84e",
  },
  {
    id: "d59a0f0e8e42ca56e58babf107958ca73d900a6e",
    short_id: "d59a0f0e",
    created_at: "2020-05-20T15:30:03.000+00:00",
    parent_ids: [
      "982d569b065ed5f1c047127c36da08b24cae0645",
      "e59d07ac46d44a8fc97d0d0660e9a954a4d53a02",
    ],
    title:
      "Merge branch 'replace-slot-for-vue3-migration-board-form' into 'master'",
    message:
      "Merge branch 'replace-slot-for-vue3-migration-board-form' into 'master'\n\nReplace slot syntax for Vue 3 migration\n\nSee merge request gitlab-org/gitlab!31987",
    author_name: "Andrew Fontaine",
    author_email: "afontaine@gitlab.com",
    authored_date: "2020-05-20T15:30:03.000+00:00",
    committer_name: "Andrew Fontaine",
    committer_email: "afontaine@gitlab.com",
    committed_date: "2020-05-20T15:30:03.000+00:00",
    web_url:
      "https://gitlab.com/gitlab-org/gitlab/-/commit/d59a0f0e8e42ca56e58babf107958ca73d900a6e",
  },
];

const firstTimeStamp = moment("2020-05-20T01:00:00");

describe("CommitIterator", () => {
  beforeEach(() => {
    getCommits
      .mockReturnValue(Promise.resolve({ data: [] }))
      .mockReturnValueOnce(Promise.resolve({ data: mockCommits.slice(0, 5) }))
      .mockReturnValueOnce(Promise.resolve({ data: mockCommits.slice(5, 10) }));
  });

  afterEach(() => {
    getCommits.mockRestore();
  });

  it("iterates through all commits", async () => {
    const commits = new CommitIterator({
      firstTimeStamp,
    });

    const all = [];

    for await (let value of commits) {
      all.push(value);
    }
    expect(all).toMatchObject(mockCommits);
  });

  it("skips to next iteration if nextIteration is called", async () => {
    const commits = new CommitIterator({
      firstTimeStamp,
    });

    const all = [];

    const MAX_LENGTH = 8;

    let count = 0;

    for await (let value of commits) {
      all.push(value);
      count += 1;
      if (count === MAX_LENGTH) {
        commits.nextIteration();
      }
    }
    expect(count).toBe(MAX_LENGTH);
    expect(all).toMatchObject(mockCommits.slice(0, MAX_LENGTH));
  });

  it("ends iteration if break is called", async () => {
    const commits = new CommitIterator({
      firstTimeStamp,
    });

    const all = [];

    const MAX_LENGTH = 3;

    let count = 0;

    for await (let value of commits) {
      all.push(value);
      count += 1;
      if (count === MAX_LENGTH) {
        break;
      }
    }
    expect(count).toBe(MAX_LENGTH);
    expect(all).toMatchObject(mockCommits.slice(0, MAX_LENGTH));
  });
});
