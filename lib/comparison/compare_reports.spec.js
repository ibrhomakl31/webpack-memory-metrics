const compareReports = require("./compare_reports");

describe("comparison", () => {
  const from = require("../__fixtures__/entry_points_before.json");
  const to = require("../__fixtures__/entry_points_after.json");

  const comparison = compareReports(from, to);

  it("compareReports matches snapshot", () => {
    expect(comparison).toMatchSnapshot();
  });
});
