const fs = require("fs");
const path = require("path");

const _ = require("lodash");
const moment = require("moment");

const CommitIterator = require("../lib/commit_iterator");
const entryPointAnalysis = require("../lib/entry_point_analysis");
const { retrieveLatestArtifact, STATUS, smartSlice } = require("../lib/common");

const SIZE_THRESHOLD = process.env.CI ? 300 : 10;

const WEBPACK_BUNDLE_SIZES_DIR = path.join(
  __dirname,
  "..",
  "webpack-bundle-sizes"
);

function findClosestReport(history) {
  const indexMap = {};

  const findIndex = (sha) => {
    if (!Number.isFinite(indexMap[sha])) {
      indexMap[sha] = history.findIndex((x) => x.id === sha);
    }
    return indexMap[sha];
  };

  let count = 0;

  const visited = new Set();

  for (let index = history.length - 1; index >= 0; index -= 1) {
    const commit = history[index];
    indexMap[commit.id] = index;
    if (commit.statsPointer || visited.has(index)) {
      continue;
    }
    if (commit.hasStats) {
      commit.statsPointer = commit.id;
      continue;
    }

    const queue = [];

    queue.push([index]);
    while (queue.length > 0) {
      count += 1;
      const indices = queue.pop();
      const currentIndex = indices[0];
      if (currentIndex < 0) {
        continue;
      }
      if (history[currentIndex].statsPointer) {
        indices.forEach((x) => {
          history[x].statsPointer = history[currentIndex].statsPointer;
        });
        break;
      }

      history[currentIndex].parent_ids.forEach((parent) => {
        const parentIndex = findIndex(parent);

        if (parentIndex >= 0 && !visited.has(parentIndex)) {
          const new_indices = [parentIndex, ...indices];
          queue.push(new_indices);
        }
      });
    }

    if (!history[index].statsPointer) {
      visited.add(index);
    }
  }
  return history;
}

function saveHistory(history) {
  const updatedHistory = _.sortBy(
    findClosestReport(_.uniqBy(history, "id")),
    "created_at"
  );

  fs.writeFileSync(
    path.join(WEBPACK_BUNDLE_SIZES_DIR, "bundle-history.json"),
    JSON.stringify(updatedHistory, null, 2)
  );

  return updatedHistory;
}

async function main() {
  let previousHistory;

  try {
    previousHistory = require("../webpack-bundle-sizes/bundle-history.json");
  } catch (e) {
    previousHistory = [];
  }

  const timestamps = previousHistory.map((x) => x.created_at).sort();

  let firstTimeStamp = moment().subtract(30, "d");

  let lastTimeStamp = moment(timestamps[0]);

  if (lastTimeStamp.isBefore(firstTimeStamp)) {
    firstTimeStamp = moment(timestamps.reverse()[0]);
    lastTimeStamp = moment(firstTimeStamp).add(60, "h");
    if (lastTimeStamp.isAfter(moment())) {
      lastTimeStamp = moment();
    }
  }

  console.log(firstTimeStamp.toISOString() + "X" + lastTimeStamp.toISOString());

  let totalCount = 0;

  const allCommits = new CommitIterator({
    firstTimeStamp: firstTimeStamp,
    lastTimeStamp: lastTimeStamp,
  });

  let history = [...previousHistory];

  let count = 0;

  for await (let commit of allCommits) {
    const { id, parent_ids, created_at } = commit;

    if (history.find((x) => x.id === id)) {
      console.log(`${id}: already has stats, skipping`);
      continue;
    }

    if (totalCount > SIZE_THRESHOLD) {
      console.log(`Analyzed more than ${SIZE_THRESHOLD} commits. Bailing`);
      break;
    }

    console.log(`${id}: Retrieving stats`);

    const { status, content } = await retrieveLatestArtifact(
      id,
      ["compile-production-assets", "gitlab:assets:compile pull-cache"],
      "webpack-report/stats.json"
    );

    if (status === STATUS.PENDING) {
      history = [...previousHistory];
      continue;
    }

    const statistics = entryPointAnalysis(content);

    history.push({
      id,
      parent_ids,
      created_at,
      hasStats: Boolean(statistics),
    });

    if (statistics) {
      const outputPath = path.join(
        WEBPACK_BUNDLE_SIZES_DIR,
        "bundleStorage",
        id + ".json"
      );
      fs.writeFileSync(
        outputPath,
        JSON.stringify({ ...commit, statistics }, null, 2)
      );
    }

    count += 1;

    history = saveHistory(history);
  }

  fs.writeFileSync(
    path.join(WEBPACK_BUNDLE_SIZES_DIR, "bundle-commits.txt"),
    history
      .flatMap((x) => (x.statsPointer ? [`${x.id}\t${x.statsPointer}`] : []))
      .join("\n")
  );

  return history;
}

main()
  .then((result) => {
    console.warn(JSON.stringify(smartSlice(result), null, 2));
    console.warn("Successfully loaded history!");
  })
  .catch((e) => {
    console.warn("An error happened!");
    console.warn(e);
    process.exit(1);
  });
