const fs = require("fs");
const path = require("path");
const axios = require("axios");
const moment = require("moment");
const CommitIterator = require("../lib/commit_iterator");
const { retrieveLatestArtifact, smartSlice } = require("../lib/common");

async function main() {
  const prevHistory = require("../webpack-bundle-sizes/memory-history.json");

  const firstTimeStamp = process.env.UNTIL
    ? moment(process.env.UNTIL)
    : moment(Math.max(...prevHistory.map((entry) => entry.date)));

  const commits = new CommitIterator({
    firstTimeStamp,
    prevTime: (curr, commits, value) => {
      return (value ? moment(value.date) : moment(curr)).subtract(12, "h");
    },
  });

  for await (let commit of commits) {
    const { content } = await retrieveLatestArtifact(
      commit.id,
      "webpack-dev-server",
      "webpack-dev-server.json"
    );

    if (content) {
      commits.nextIteration();
      prevHistory.push(content);
    }
  }

  const history = Object.values(
    prevHistory.reduce((res, el) => {
      res[el.commitSHA] = el;

      return res;
    }, {})
  );

  fs.writeFileSync(
    path.join(__dirname, "..", "webpack-bundle-sizes", "memory-history.json"),
    JSON.stringify(history, null, 2)
  );

  let prevDate = null;
  const lastIndex = history.length - 1;

  const reducedHistory = prevHistory
    .flatMap((entry, index) => {
      if (
        index < lastIndex &&
        prevDate &&
        entry.date < prevDate + 24 * 60 * 60 * 1000
      ) {
        return [];
      }
      prevDate = entry.date;
      return [entry];
    })
    .sort((a, b) => a.date - b.date);

  fs.writeFileSync(
    path.join(__dirname, "..", "public", "history-memory-size.json"),
    JSON.stringify(reducedHistory, null, 2)
  );

  return reducedHistory;
}

main()
  .then((result) => {
    console.warn(JSON.stringify(smartSlice(result), null, 2));
    console.warn("Successfully loaded history!");
  })
  .catch((e) => {
    console.warn("An error happened!");
    console.warn(e);
    process.exit(1);
  });
